import { Injectable } from "@angular/core";
import { DropdownQuestion } from "./question-dropdown";
import { QuestionBase } from "./question-base";
import { TextboxQuestion } from "./question-textbox";
import { of } from "rxjs";

@Injectable()
export class QuestionService {

  // TODO: get from a remote source of question metadata
  getQuestions() {

    const questions: QuestionBase<string>[] = [

        new DropdownQuestion({
            key: 'key',
            label: 'label',
            options: [
                {key: 'webssr', value: 'web app rendu côté serveur'},
                {key: 'webssg', value: 'web app rendu côté client '},
                {key: 'mobile', value: 'application mobile'},
                {key: 'desktop', value: 'application de bureau'}
            ],
            order: 3
        }),

      new TextboxQuestion({
        key: 'firstName',
        label: 'Firstname',
        value: 'Nom',
        required: true,
        order: 1
      }),

      new TextboxQuestion({
        key: 'emailAddress',
        label: 'Email',
        type: 'email',
        order: 2
      })
    ];

    return of(questions.sort((a, b) => a.order - b.order));
}
}