import { Component, OnInit  } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CourseItem } from './store/models/courseItem.model';
import { AppState } from './store/models/app-state.model';
import { QuestionService } from './question.service';
import { QuestionBase } from './question-base';
/* import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations'; */
//import { slideInAnimation } from './animations';
                                           
@Component({
  selector: 'app-root',
   template: `
    <div>
      <h2>Contact Code Dev Design Applications</h2>
      <app-dynamic-form [questions]="questions$ | async"></app-dynamic-form>
    </div>
  `,
  providers:  [QuestionService]
})
export class AppComponent {
  questions$: Observable<QuestionBase<any>[]>;

  constructor(service: QuestionService) {
    this.questions$ = service.getQuestions();
  }
}
  /* templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'] 
})
export class AppComponent implements OnInit {
  
  courseItems$!: Observable<Array<CourseItem>>;
  
  constructor(private store: Store<AppState>) {}
  ngOnInit(): void {
    this.courseItems$ = this.store.select((store) => store.course);
  }
}
  */
